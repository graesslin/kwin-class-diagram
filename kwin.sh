#!/bin/sh
OUTPUT=kwin.uml
PLANTUML=${1}
echo "@startuml" > $OUTPUT
echo "!define ALL" >> $OUTPUT
cat qobject.txt >> $OUTPUT
cat glutils.txt >> $OUTPUT
cat libkwineffects.txt >> $OUTPUT
cat libdecoration.txt >> $OUTPUT
cat tabbox.txt >> $OUTPUT
cat paintredirector.txt >> $OUTPUT
cat scripting.txt >> $OUTPUT
cat core.txt >> $OUTPUT
cat compositor.txt >> $OUTPUT
cat openglcompositor.txt >> $OUTPUT
cat aurorae.txt >> $OUTPUT
echo "" >> $OUTPUT
echo "hide empty members" >> $OUTPUT
echo "" >> $OUTPUT
echo "@enduml" >> $OUTPUT

java -jar $PLANTUML $OUTPUT
rm $OUTPUT
